---
title: Woensdag 30 augustus
date: 2017-08-30
---

Woensdag 30 augustus
Tijdens de eerste les week hebben we een naam en een logo bedacht voor onze projectgroep. Samen hadden wij gebrainstormd en het idee kwam om twee hoeden te maken als logo. Om ons team te representeren vonden wij het een leuk idee om een nette hoed te maken maar ook een vrolijke gekke versierde hoed. Hiermee willen we laten zien met de nette hoed dat we serieus en netjes kunnen zijn en met de andere hoed ook creatief en out of the box denken. Daarbij hadden wij de groepsnaam ‘Mad Hatters’ bedacht. 

