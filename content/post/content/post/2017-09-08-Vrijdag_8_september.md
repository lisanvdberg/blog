---
title: Vrijdag 8 september
date: 2017-09-08
---
Vrijdag 8 september
Vandaag zijn we begonnen met het maken van een planning en een moodboard. Achteraf bleek dat ik en me groepje de opdracht verkeerd hadden begrepen. We hadden de foto's gebruikt die we hadden gemaakt tijdens de stadstour, de foto's van de stad. En we hadden geen moodboard van onze gekozen doelgroep. Maar ondanks dit was de feedback van de docent positief en vond ze het interessant om te zien dat wij als groep dit zo anders hadden opgevat. Ook hebben we een team verdeling gemaakt. Verder hebben wij een korte workshop gehad over bloggen en wat informatie over moodboards. Het maken van moodboards had ik eerst geïnterpreteerd als online plaatjes, kleuren en tekst bij elkaar zoeken en dit afdrukken. Maar een veel leuker idee is om dit echt met materialen te doen, zodat je de echte feel kan overbrengen. 
