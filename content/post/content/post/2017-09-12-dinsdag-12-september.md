---
title: Dinsdag 12 september
date: 2017-09-12
---



Ik heb vandaag een infographic gemaakt op basis van de enquête die ik heb afgelegd om meer te weten te komen over de doelgroep. Nadat ik feedback heb ontvangen over de docent ben ik verder gegaan met het uitwerken van mijn collage. Ik moest van de docent nog 5 steekwoorden toevoegen en 5 afbeeldingen die dit ondersteunen. De afbeeldingen had ik uiteindelijk gevonden. Daarna ben ik met mijn teamgenoot begonnen met het schetsen van de app die wij in gedachte hadden. Tijdens het maken van de schetsen hebben wij gebrainstormd en dit was een hele goede sessie. Nadat dit was uitgetekend heb ik en mijn teamgenoot het aan de rest van het team laten zien. Hierdoor kwamen we erachter dat er nog wat punten miste. En na een brainstormsessie samen met mijn teamgenoten kwamen we uiteindelijk op meer aanpassingen en nieuwe ideeën. Ik kwam met het idee om een dansmat toe te voegen aan ons muziekspel.
