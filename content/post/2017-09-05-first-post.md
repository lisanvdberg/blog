---
title: Dinsdag 5 september
date: 2017-09-05
---


Vandaag zijn we begonnen met het brainstormen over onze doelgroep voor de design challenge. We hebben gekozen voor de doelgroep CMD studenten. Daarna zijn we begonnen met het brainstormen over hoe het spel er uit moet komen te zien. Onze eerste gedachten was een soort tour door rotterdam. Omdat we niet echt wisten wat een leuk idee was hebben we besloten om 3 verschillende ideeën uit te werken op papier. 
-Het eerste idee was om een spel te maken door het tramsysteem van Rotterdam (London metro game).
-Het tweede idee was om met een muziekspel plaatsen in rotterdam te ontgrendelen.
-Het derde idee was om punten te verzamelen door middel van het vinden van locaties en daarmee punten te verbinden waar uiteindelijk een afbeelding uit voorkomt. 

Ik en mijn team waren er mee eens om het thema muziek aan te houden, omdat dit volgens onderzoeken mensen bij elkaar brengt en dat is van onze doelen. Ook hebben we gebrainstormd over een naam van het spel, dit hebben we gedaan door woorden die bij ons op kwamen. Deze woorden hebben we samengevoegd en uiteindelijk kwamen we op het idee ‘010’. Daarna hebben we samen een vragenlijst samengesteld en zijn we de leerlingen langs gegaan uit onze klas en hebben zij deze voor ons onderzoek beantwoordt. 

Verder heb ik vandaag een samenwerkingsovereenkomst gemaakt en de contactgegevens van de projectleden genoteerd maar ook teamafspraken de uitslagen van de Belbin test en een planning. Vervolgens heb ik dit doorgenomen met me projectleden en hier waren ze het mee eens. 
